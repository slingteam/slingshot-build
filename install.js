var appRoot = require('app-root-path');
var fs = require('fs-extra');

// copy files to project working directory
['gulpfile.js', 'gulp-conf.js'].forEach(file => {
  fs.copy(file, appRoot + '/' + file, err => {
    if (err) console.log(err);
  });
});
