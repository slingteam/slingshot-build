import path from 'path';
import fs from 'fs';
import events from 'events';

/**
 * Carrega todas as tasks disponíveis nas pastas "tasks"
 */
module.exports = (gulp, conf) => {
  const eventEmitter = new events.EventEmitter();

  [path.join(__dirname, 'tasks'), path.join(process.cwd(), 'tasks')].forEach((dir) => {
    try {
      fs.readdirSync(dir).forEach((file) => {
        if (file.split('.').pop() === 'js') {
          require(path.join(dir, file))(gulp, conf, eventEmitter);
        }
      });
    } catch (e) {}
  });
};
