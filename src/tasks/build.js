import runSequence from 'run-sequence';

/**
 * Build files for production
 */
module.exports = gulp => {
  gulp.task('build', ['clean'], (cb) => {
    runSequence.use(gulp)(['js', 'styles', 'images', 'fonts'], 'revision', cb);
  });
};
