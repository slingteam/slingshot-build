module.exports = gulp => {
  gulp.task('dist', () => {
    gulp.start('build');
    gulp.start('php-cs-fixer');
  });
};
