import del from 'del';
import fs from 'fs';
import rev from 'gulp-rev';

module.exports = (gulp, conf) => {
  /**
   * Clean rev-manifest files
   */
  gulp.task('revision:clean', () => {
    const manifestPath = conf.assetsPath + 'rev-manifest.json';

    try {
      fs.statSync(manifestPath);
      var manifest = JSON.parse(fs.readFileSync(manifestPath));
      var filesToDelete = Object.keys(manifest).map(key => conf.publicPath + manifest[key]);

      del.sync(filesToDelete, {force: true});
    } catch (e) {}
  });

  /**
   * File revision
   */
  gulp.task('revision', ['revision:clean'], () => {
    return gulp.src(
        [
          conf.assetsPath + conf.cssPath + '**/*.css',
          conf.assetsPath + conf.jsPath + '**/*.js'
        ],
        {base: conf.publicPath}
      )
      .pipe(rev())
      .pipe(gulp.dest(conf.publicPath))
      .pipe(rev.manifest())
      .pipe(gulp.dest(conf.assetsPath));
  });
};
