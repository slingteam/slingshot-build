import cache from 'gulp-cache';

/**
 * Clears cache
 */
module.exports = gulp => {
  gulp.task('cache:clear', cb => cache.clearAll(cb));
};
