import gutil from 'gulp-util';
import postcss from 'gulp-postcss';
import sass from 'gulp-sass';
import size from 'gulp-size';
import sourcemaps from 'gulp-sourcemaps';

module.exports = (gulp, conf) => {
  function postcssProcessors() {
    const processors = conf.postcssProcessors || {};
    gutil.log('Postcss running with processors:');
    return Object.keys(processors).map(plugin => {
      // load plugin
      gutil.log(gutil.colors.blue(plugin));
      const options = processors[plugin];
      return options === false ? require(plugin) : require(plugin)(options);
    });
  }

  gulp.task('styles:scss', function () {
    return gulp.src(conf.devAssetsPath + conf.sassPath + '**/*.{sass,scss}')
      .pipe(sourcemaps.init())
      .pipe(sass({
        'precision': 10
      }).on('error', sass.logError))
      .pipe(postcss(postcssProcessors()))
      .pipe(size({title: 'css pre-sourcemaps'}))
      .pipe(sourcemaps.write('.', {sourceRoot: conf.devAssetsPath + conf.sassPath}))
      .pipe(gulp.dest(conf.assetsPath + conf.cssPath))
      .pipe(size({title: 'css post-sourcemaps'}));
  });

  // Output Final CSS Styles
  gulp.task('styles', ['styles:scss']);
};
