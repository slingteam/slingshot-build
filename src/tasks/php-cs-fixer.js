import shell from 'gulp-shell';

module.exports = (gulp, conf) => {
  gulp.task('php-cs-fixer', shell.task([
    './vendor/bin/php-cs-fixer fix ' + conf.phpCsFixPath
  ], {
    ignoreErrors: true
  }));
};
