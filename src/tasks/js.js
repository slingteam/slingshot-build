import fs from 'fs';
import gutil from 'gulp-util';
import {mergeWith, isArray} from 'lodash';
import size from 'gulp-size';
import webpack from 'webpack';

module.exports = (gulp, conf, eventEmitter) => {
  const watch = gutil.env._.indexOf('serve') > -1;

  function configuration() {
    let config,
      defaultConfig = {
        output: {
          path: conf.assetsPath + conf.jsPath,
          filename: '[name].js'
        },
        watch: watch,
        watchOptions: {
          poll: true,
          aggregateTimeout: 500,
          ignored: /node_modules/
        },
        module: {
          preLoaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'jshint-loader'
          }],
          loaders: [{
            test: /\.js$/,
            loader: 'buble-loader'
          }]
        },
        plugins: [
          new webpack.optimize.UglifyJsPlugin({minimize: true, sourceMap: true})
        ]
      };

    if (fs.existsSync('webpack.config.js')) {
      config = require(process.cwd() + '/webpack.config.js');
    }

    return mergeWith(
      defaultConfig,
      conf.webpack,
      config,
      (objValue, srcValue) => {
        if (isArray(objValue)) {
          return objValue.concat(srcValue);
        }
      }
    );
  }

  gulp.task('js', (cb) => {
    webpack(configuration(), function (err, stats) {
      if (err) throw new gutil.PluginError('webpack', err);

      if (stats.hasErrors() || stats.hasWarnings()) {
        gutil.log(stats.toString('errors-only'));
      }

      if (watch) {
        gutil.log(gutil.colors.blue('webpack is watching for changes...'));
      }

      eventEmitter.emit('webpack-compiled');

      if (cb) {
        cb();
        cb = null;
      }
    });
  });
};
