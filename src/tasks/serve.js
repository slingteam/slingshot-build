import browserSync from 'browser-sync';
import runSequenceModule from 'run-sequence';

module.exports = (gulp, conf, eventEmitter) => {
  const runSequence = runSequenceModule.use(gulp);
  const watch = gulp.watch;

  // Browsersync
  const bsEnabled = conf.bsEnabled || false;
  const bsReload = bsEnabled ? browserSync.reload : () => {};


  /**
   * Watch Files For Changes & Reload
   */
  gulp.task('serve', ['build'], () => {
    if (bsEnabled) {
      browserSync({
        proxy: conf.bsProxy,
        open: conf.bsOpen
      });

      watch(conf.templatesPath + '**/*' + conf.templatesExt).on('change', () => {
        bsReload();
      });
    }
    watch(conf.devAssetsPath + conf.sassPath + '**/*.{sass,scss}').on('change', () => {
      runSequence('styles', 'revision', bsReload);
    });
    watch(conf.devAssetsPath + conf.imgPath + '**/*.{jpg,jpeg,gif,png,svg}').on('change', () => {
      runSequence('images', bsReload);
    });
    watch(conf.devAssetsPath + conf.fontsPath + '**').on('change', () => {
      runSequence('fonts', bsReload);
    });
    eventEmitter.on('webpack-compiled', function () {
      runSequence('revision', bsReload);
    });
  });
};
