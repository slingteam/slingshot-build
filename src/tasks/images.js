import cache from 'gulp-cache';
import imagemin from 'gulp-imagemin';
import pngquant from 'imagemin-pngquant';
import size from 'gulp-size';

module.exports = (gulp, conf) => {
  const pngquantConf = conf.pngquant || {quality: '70-85', speed: 4};
  const svgoConf = conf.svgo || [{removeUselessDefs: false}, {cleanupIDs: false}];

  /**
   * Optimize Images
   */
  gulp.task('images', () => {
    return gulp.src(conf.devAssetsPath + conf.imgPath + '**/*.{jpg,jpeg,gif,png,svg}')
      .pipe(size({title: 'images before'}))
      .pipe(cache(imagemin([
        imagemin.gifsicle({interlaced: true}),
        imagemin.jpegtran({progressive: true}),
        pngquant(pngquantConf),
        imagemin.svgo(svgoConf)
      ])))
      .pipe(gulp.dest(conf.assetsPath + conf.imgPath))
      .pipe(size({title: 'images after'}));
  });
};
