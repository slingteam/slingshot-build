import size from 'gulp-size';

module.exports = (gulp, conf) => {
  /**
   * Copy Web Fonts To Dist
   */
  gulp.task('fonts:copy', () => {
    return gulp.src(conf.devAssetsPath + conf.fontsPath + '**')
      .pipe(gulp.dest(conf.assetsPath + conf.fontsPath))
      .pipe(size({title: 'fonts'}));
  });

  gulp.task('fonts', ['fonts:copy']);
};
