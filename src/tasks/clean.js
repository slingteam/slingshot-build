import del from 'del';

/**
 * Cleans output directory
 * @param gulp
 * @param conf
 */
module.exports = (gulp, conf) => {
  gulp.task('clean', () => {
    del.sync([
      conf.assetsPath + conf.cssPath,
      conf.assetsPath + conf.fontsPath,
      conf.assetsPath + conf.imgPath,
      conf.assetsPath + conf.jsPath,
      conf.assetsPath + 'rev-manifest.json'
    ]);
  });
};
