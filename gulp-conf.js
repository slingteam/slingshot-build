module.exports = {
  publicPath: './public_html/',
  devAssetsPath: 'resources/assets/',
  get assetsPath() {
    return this.publicPath + 'assets/';
  },
  templatesPath: 'resources/views/',
  templatesExt: '.blade.php',
  fontsPath: 'fonts/',
  jsPath: 'js/',
  imgPath: 'img/',
  sassPath: 'sass/',
  cssPath: 'css/',
  bsProxy: 'yoursite.dev',
  phpCsFixPath: 'app',
  bsEnabled: false,
  bsOpen: false,
  postcssProcessors: {
    autoprefixer: {
      browsers: ['last 2 versions', 'ie 9']
    },
    'postcss-csso': false
  },
  webpack: {
    entry: {
      main: './resources/assets/js/main.js'
    },
    devtool: 'source-map'
  }
};
