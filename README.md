# Slingshot build

Provides several tasks to compile and optimize your project.

Requires `Gulp` installed locally.

After the installation a `gulpfile.js` is created locally. You can run `gulp -T` to list the tasks available.

## Postcss plugins

Postcss plugins can be installed and configured in the `gulp-conf.js` file under the `postcssProcessors` name.

Currently Slingshot build brings the autoprefixer and cssnano plugins.

Go to [http://postcss.parts/](http://postcss.parts/) to search for more plugins.
